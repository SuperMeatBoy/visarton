function validateTel(number) {
    var re = /^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?$/;
    return re.test(String(number));
}
$( document ).ready(function() {
    /*menu*/
    $('.header__toggle-menu').on('click', function () {
        var $this = $(this);
        $this.closest('.header').toggleClass('menu-open');
    });
    $('.language button').on('click', function () {
        var $this = $(this);
        var $val = $this.text();

        $this.closest('.language').attr('data-language', $val);
    });

    /*team*/
    $('.btn--team').on('click', function (){
        $(this).closest('.about__preview').addClass('hide');
    });

    /*fullPage*/
    let fullPage = function (options) {
        let $element = $(options.init);
        let $whoScroll;

        if (!$element.length) return;
        $element.slick(options.options);

        let footerSlide = $('.slider .slick-slide:last-child');
        let $slickDots = $('.slick-dots');
        $slickDots.append(`
                                <li role="presentation">
                                    <button type="button" id="slick-slide-control09">10</button>
                                </li>
                                `);
        var timer;
        $element.on('mousewheel', '.slick-track', function(e) {
            e.preventDefault();
            if(timer) {
                window.clearTimeout(timer);
            }

            timer = window.setTimeout(function() {
                if (e.deltaY > 0) {
                    $whoScroll = true;
                    if(!footerSlide.hasClass('step-two')){
                        $element.slick('slickPrev');
                    }
                } else {
                    $element.slick('slickNext');
                    $whoScroll = false;
                }

                footer($whoScroll);
                stepAnimation($element);
            }, 100);
        });

        $('.js-slider .slick-dots li').click(function () {
            let $index = $(this).index();
            if($index === 8){
                footerSlide.addClass('step-one').removeClass('step-two');
            }else if($index === 9){
                $element.slick("slickGoTo", 8);
                footerSlide.addClass('step-one');
                footer(false);
            } else{
                footerSlide.removeClass('step-one step-two');
            }
            stepAnimation($element);
        });
    };
    fullPage({
        init: '.js-slider',
        options: {
            infinite: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            vertical: true,
            verticalSwiping: true,
            //initialSlide: 7,
            dots: true,
            arrows: false,
            responsive: [
                {
                    breakpoint: 1200,
                    settings: "unslick"
                }
            ]
        }
    });

    /*initSlider*/
    let initSliders = function (options) {
        let $element = $(options.init);

        if (!$element.length) return;
        $element.slick(options.options);

    };

    /*Offer*/
    initSliders({
        init: '.js-offer-slider',
        options: {
            asNavFor: '.js-offer-slider-nav',
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            speed: 0,
            fade: true,
            dots: false,
            arrows: true,
            prevArrow:'<button class="slick-arrow slick-prev" type="button" data-svg="img/svg/arrow-left-long.svg"></button>',
            nextArrow:'<button class="slick-arrow slick-next" type="button" data-svg="img/svg/arrow-right-long.svg"></button>',
        }
    });
    $('.js-offer-slider').on('afterChange', function(event, slick, currentSlide, nextSlide){
        $('.js-offer-slider').removeClass('animate--true');
        setTimeout(function () {
            $('.js-offer-slider').addClass('animate--true');
        }, 500);
    });
    initSliders({
        init: '.js-offer-slider-nav',
        options: {
            asNavFor: '.js-offer-slider',
            infinite: true,
            slidesToShow: 7,
            slidesToScroll: 1,
            draggable: false,
            vertical: true,
            verticalSwiping: true,
            focusOnSelect: true,
            centerMode: true,
            dots: false,
            arrows: false,
            responsive: [
                {
                    breakpoint: 992,
                    settings: "unslick"
                }
            ]
        }
    });
    $('.js-offer-slider-nav .slick-slide[data-slick-index="-5"]').addClass('hide');
    $('.js-offer-slider-nav').on('afterChange', function(){
        let el = $('.js-offer-slider-nav .slick-current');
        $('.js-offer-slider-nav .slick-slide').removeClass('hide');
        el.prev().prev().prev().prev().prev().addClass('hide');
        el.next().next().next().next().next().addClass('hide');
    });


    /*Advantage*/
    initSliders({
        init: '.js-advantages-slider',
        options: {
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: false,
            arrows: false,
            autoplay: true,
            autoplaySpeed: 3000
        }
    });
    $('.js-advantage-nav').on('click', function (){
        let $this = $(this);
        let $index = $this.parent().index();

        $('.js-advantage-nav').removeClass('advantage--open');
        $('.js-advantages-slider').slick('slickGoTo', $index);
        $this.addClass('advantage--open');
    });
    $('.js-advantages-slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
        $('.js-advantage-nav').removeClass('advantage--open');
        $('.advantages-nav-list > div').eq(nextSlide).find('.js-advantage-nav').addClass('advantage--open');
    });


    /*Portfolio*/
    initSliders({
        init: '.js-partners-slider-list',
        options: {
            infinite: false,
            slidesToShow: 3,
            slidesToScroll: 1,
            dots: false,
            arrows: true,
            prevArrow:'<button class="slick-arrow slick-prev" type="button" data-svg="img/svg/arrow-left-long.svg"></button>',
            nextArrow:'<button class="slick-arrow slick-next" type="button" data-svg="img/svg/arrow-right-long.svg"></button>',
            responsive: [
                {
                    breakpoint: 580,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        }
    });
    initSliders({
        init: '.js-portfolio-slider',
        options: {
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            dots: false,
            arrows: true,
            prevArrow:'<button class="slick-arrow slick-prev" type="button" data-svg="img/svg/arrow-left-short.svg"></button>',
            nextArrow:'<button class="slick-arrow slick-next" type="button" data-svg="img/svg/arrow-right-short.svg"></button>'
        }
    });
    $('.portfolio-slider img').on('click', function (){
        let $this = $(this);
        let $src = $this.attr('src');

        $('.projects__preview').css({"background-image": "url("+$src+")"});
    });

    /*tabClick*/
    $('.tab__list [data-tab]').on('click', function () {
        var $this = $(this);
        var $name = $this.data('tab');
        var $id = $this.closest('.tab__list').data('tab-id');
        var $idEl = $('.tab__body[data-tab-id="'+ $id +'"]');

        $this.closest('.tab__list').find('[data-tab]').removeClass('active');
        $this.addClass('active');
        $idEl.children('[data-tab]').hide();
        $idEl.children('[data-tab="'+ $name +'"]').show();

        if($id === "partners"){
            let $imgSrc = $idEl.children('[data-tab="'+ $name +'"]').find(".js-portfolio-slider .slick-slide:first-child img").attr('src');

            $idEl.children('[data-tab="'+ $name +'"]').find('.projects__preview').css({"background-image": "url("+ $imgSrc +")"});
            $idEl.children('[data-tab="'+ $name +'"]').find('.tab__list > li:first-child').trigger('click');
        }

        if($id === "projects"){
            let $imgSrc = $idEl.children('[data-tab="'+ $name +'"]').find(".js-portfolio-slider .slick-slide[data-slick-index='0'] img").attr('src');

            $this.closest('.projects').find('.projects__preview').css({"background-image": "url("+ $imgSrc +")"});
            $this.closest('.projects__info').find('.projects__more').data('src', '#'+ $name +'');
        }

        if($('.js-portfolio-slider').length){
            $('.js-portfolio-slider').slick("setPosition", 0);
        }
        animateAll();
    });

    $('[data-fancybox]').fancybox({
        autoFocus: false,
    });

    /*loadSvg*/
    $('[data-svg]').each(function(){
        var $this = $(this);
        var $svg = $this.data('svg');
        var $filename = $svg.split('\\').pop().split('/').pop().replace(".svg", "");

        $this.load($svg, function(responseTxt, statusTxt){
            if(statusTxt == "success"){
                $this.find('svg').addClass('svg svg-'+$filename+'');
            }
        });
    });
    thumbImg();
    animateAll();
    // $("input[type='tel']").mask("+ 999 999-99-99");

    if($(window).width() > 1200 ){
        let $element = $('.js-slider');
        let footerSlide = $('.slider .slick-slide:last-child');
        $('[data-go-slide]').on('click', function (e) {
            e.preventDefault();
            let $this = $(this);
            let $index = $this.data('go-slide');

            if($index === 'footer'){
                $element.slick("slickGoTo", 8);
                footerSlide.addClass('step-one');
                footer(false);
            }else {
                $element.slick("slickGoTo", $index);
                footerSlide.removeClass('step-one step-two');
            }
            $('.header').removeClass('menu-open');
            stepAnimation($element);
        });
    }else{
        $('a[href*="#"]')
            .not('[href="#"]')
            .not('[href="#0"]')
            .on('click tap',function(event) {
                if (
                    location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
                    &&
                    location.hostname == this.hostname
                ){
                    var target = $(this.hash);
                    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                    if (target.length) {
                        event.preventDefault();
                        $('.header').removeClass('menu-open');
                        $('html, body')
                            .animate({
                                scrollTop: target.offset().top
                            }, 1000);
                    }
                }
            });
    }
});

function thumbImg() {
    $('[data-thumb]').each(function () {
        var $this = $(this);
        var img = $this.find('img').attr('src');
        var size = $this.data('thumb');
        $this.css({
            'background-image': 'url(' + img + ')',
            'background-size': '' + size + ''
        });
    });
    return false;
}

function animateAll(){
    let $target = $('.slick-current.slick-active [data-animate]');

    $('[data-animate]').removeClass('animated');

    $target.addClass('animated');
}

function footer($whoScroll) {
    let footerSlide = $('.slider .slick-slide:last-child');
    let $slickDots = $('.slick-dots');

    if(footerSlide.hasClass('step-two') && $whoScroll === true){
        footerSlide.removeClass('step-two');
        $slickDots.find('li').removeClass('slick-active');
        $slickDots.find('li:nth-child(9)').addClass('slick-active');
    }else if(footerSlide.hasClass('step-one') && $whoScroll === false){
        footerSlide.addClass('step-two');
        $slickDots.find('li').removeClass('slick-active');
        $slickDots.find('li:last-child').addClass('slick-active');
    } else if(footerSlide.hasClass('slick-active') && $whoScroll === false){
        footerSlide.addClass('step-one');
    } else {
        footerSlide.removeClass('step-one');
    }
}

function stepAnimation($element){
    /*animation*/
    let currentSlide =  $element.slick('slickCurrentSlide');
    switch (currentSlide) {
        case 1:
            $('.js-offer-slider-nav').slick("slickGoTo", 0);
            $('.js-offer-slider').addClass('animate--true');
            break;
        case 2:
            let $selector =  $('.processes__list > div');
            let $length =  $selector.length;
        function timeout(counter) {
            if(counter < $length){
                setTimeout(function(){
                    $selector.eq(counter).addClass('animation--true animation--svg');
                    counter++;
                    timeout(counter);
                }, 500);
            }
        }
            timeout(0);

            $selector.hover(
                function() {
                    let $this = $(this);
                    $this.removeClass('svg-icon animation--svg');
                    $this.addClass('svg-icon');
                    setTimeout(function(){
                        $this.addClass('animation--svg');
                    }, 1500)
                }, function() {

                }
            );
            break;
        default:
    }
}

$(document).on('click','.service .btn',function() {
    var $this = $(this);
    var titleText = $this.text();
    $("#tariff .title").text("Package name / " + titleText);
    $("#tariff").find("[name=currentTariff]").val(titleText);
});
var fileError = 0;
$(document).on('submit','.formTheme',function(e){
    e.preventDefault();
    var form = $(this);
    var request = ajaxForm(this);
    var $button = form.find('button[type=submit]');
    if(fileError != 1) {
        $button.prop('disabled',true);
        request.done(function(msg) {
            var mes = msg.message,
                status = msg.status;
            form.trigger('reset');
            $.fancybox.close();
            $('.fileName').text("(не более 15 Мб)");
            $.fancybox.open($("#thanks"));
        });
        request.fail(function(jqXHR, textStatus) {
            alert("Произошла ошибка: " + textStatus);
            $button.prop('disabled',false);
        });
    }
    return false;
});
function ajaxForm(form) {
    var url = 'handler.php';
    var $button = $(form).find('button[type=submit]');
    return $.ajax({
        url: url,
        type: "POST",
        data:  new FormData(form),
        contentType: false,
        cache: false,
        processData:false,
        success: function(data){
            yaCounter53894170.reachGoal('ORDER');
            $button.prop('disabled',false);
        },
        error: function(){
            $button.prop('disabled',false);
        }
    });
}
function clearFileInputField(Id) {
    document.getElementById(Id).value = "";
}

$(document).on('change','.fileWrapper',function () {
    var $this = $(this);
    var $input = $this.find('.input--file');
    var iSize = ($input[0].files[0].size / 1024);
    var SizeTrue = iSize;
    var $filename = $input.val().split('\\').pop().split('/').pop();
    if (iSize / 1024 > 1) {
        if (((iSize / 1024) / 1024) > 1)
        {
            iSize = (Math.round(((iSize / 1024) / 1024) * 100) / 100);
            console.log(iSize + "Gb");
            fileError = 1;
        }
        else
        {
            iSize = (Math.round((iSize / 1024) * 100) / 100)
            console.log(iSize + "Mb");
            if(parseInt(SizeTrue) > 15000) {
                fileError = 1;
            }
        }
    }
    else
    {
        iSize = (Math.round(iSize * 100) / 100)
        fileError = 0;
    }
    if(fileError == 1) {
        $(".formStatus").text("Your file is too big!");
        clearFileInputField("attachment");
        $('.fileName').text("(No more than 15 Mb)");
        fileError = 0;
    }
    else{
        $(".formStatus").text("");
        $this.find('.fileName').text($filename);
    }
});
$(document).on('change','input[name=agree]',function () {
    var $this = $(this);
    var $form = $(this).closest('form');
    if($this.prop('checked') == false){
        $form.find('button[type=submit]').prop('disabled',true);
    }
    else {
        $form.find('button[type=submit]').prop('disabled',false);
    }
});
$(document).on('keypress',"input[type='tel']",function(event){
    var $this = $(this);
    if (event.charCode && (event.charCode < 48 || event.charCode > 57)){
        return false;
    };
});
